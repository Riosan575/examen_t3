﻿using Examen_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Patron
{
    interface IRutinas
    {
        public List<RutinaDetalle> ElegirRutina(int idRutina, List<Ejercicios> ejercicios);
    }
}
