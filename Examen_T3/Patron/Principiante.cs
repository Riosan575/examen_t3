﻿using Examen_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Patron
{
    public class Principiante : IRutinas
    {
        public List<RutinaDetalle> ElegirRutina(int idRutina, List<Ejercicios> ejercicios)
        {
            Random random = new Random();
            List<RutinaDetalle> rutinaDetalles = new List<RutinaDetalle>();
            for (int i = 0; i < 5; i++)
            {
                var detalle = new RutinaDetalle();
                var ejercicio = random.Next(1, ejercicios.Count());

                detalle.IdEjercicios = ejercicio;
                detalle.IdRutinaUsuario = idRutina;
                detalle.Tiempo = random.Next(60, 120);
                rutinaDetalles.Add(detalle);
            }
            return rutinaDetalles;
        }
    }
}
