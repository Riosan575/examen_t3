﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Models
{
    public class Ejercicios
    {
        public int Id { get; set; }
        public string NombreEje { get; set; }
        public string VideoYT { get; set; }
    }
}
