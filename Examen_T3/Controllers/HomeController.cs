﻿using Examen_T3.DB;
using Examen_T3.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Controllers
{
    public class HomeController : Controller
    {
        private T3Context context;
        public HomeController(T3Context context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var claim = HttpContext.User.Claims.First();
                string username = claim.Value;
                var user = context.Accounts.First(o => o.Usuario == username);
                ViewBag.Rutina = context.Rutinas.Where(o => o.IdUsuario == user.Id).ToList();
                return View();
            }      
            return View();
        }
        public IActionResult Detalle(int id) {

            var rutina = context.Rutinas.First(o => o.Id == id);

            ViewBag.Ejercicios = context.RutinaDetalles.Include("Ejercicios").Where(o => o.IdRutinaUsuario == id).ToList();

            return View(rutina);
        }
    }
}
