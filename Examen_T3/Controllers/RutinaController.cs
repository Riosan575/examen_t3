﻿using Examen_T3.DB;
using Examen_T3.Models;
using Examen_T3.Patron;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Controllers
{
    [Authorize]
    public class RutinaController : Controller
    {
        private T3Context context;
        private IRutinas rutinas;

        public RutinaController(T3Context context)
        {
            this.context = context;
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View(new Rutina());
        }
        [HttpPost]
        public IActionResult Create(Rutina rutina)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Accounts.First(o => o.Usuario == username);

            rutina.IdUsuario = user.Id;
            if (ModelState.IsValid)
            {
                context.Rutinas.Add(rutina);
                context.SaveChanges();

                if (rutina.Id != 0)
                {
                    var ejercicios = context.Ejercicios.ToList();

                    if(rutina.Tipo == "Principiante")
                    {
                        rutinas = new Principiante();
                    }
                    else if (rutina.Tipo == "Intermedio")
                    {
                        rutinas = new Intermedio();
                    }
                    else if (rutina.Tipo == "Avanzado")
                    {
                        rutinas = new Avanzado();
                    }

                    var aplico = rutinas.ElegirRutina(rutina.Id, ejercicios);
                    context.RutinaDetalles.AddRange(aplico);
                    context.SaveChanges();
                }
                return RedirectToAction("Index", "Home");
            }
            return View(new Rutina());
        }
    }
}
