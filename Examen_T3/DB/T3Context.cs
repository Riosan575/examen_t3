﻿using Examen_T3.DB.Mapping;
using Examen_T3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.DB
{
    public class T3Context: DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Ejercicios> Ejercicios { get; set; }
        public DbSet<Rutina> Rutinas { get; set; }
        public DbSet<RutinaDetalle> RutinaDetalles { get; set; }
        public T3Context(DbContextOptions<T3Context> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new AccountMap());
            modelBuilder.ApplyConfiguration(new EjerciciosMap());
            modelBuilder.ApplyConfiguration(new RutinaMap());
            modelBuilder.ApplyConfiguration(new RDetalleMap());
        }
    }
}
