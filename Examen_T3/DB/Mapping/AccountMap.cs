﻿using Examen_T3.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.DB.Mapping
{
    public class AccountMap: IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.ToTable("Account");
            builder.HasKey(o => o.Id);

            //builder.HasMany(o => o.Books)
            //    .WithOne(o => o.Autor)
            //    .HasForeignKey(o => o.AutorId);
        }
    }
}
