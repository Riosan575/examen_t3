﻿using Examen_T3.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.DB.Mapping
{
    public class RDetalleMap : IEntityTypeConfiguration<RutinaDetalle>
    {
        public void Configure(EntityTypeBuilder<RutinaDetalle> builder)
        {
            builder.ToTable("RutinaDetalle");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Ejercicios)
                .WithMany()
                .HasForeignKey(o => o.IdEjercicios);
        }
    }
}
